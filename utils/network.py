import socket
from PyQt5.QtWidgets import QProgressDialog
from PyQt5.QtCore import Qt
from PyQt5.Qt import Qt

class NetworkSocket():

    def __init__(self, ip):
        self.network = socket.socket()
        self.network.connect((ip,7077))

    def sendMetaData(self,filename):
        self.network.send(bytearray(filename, 'UTF-8'))
        bmsg = self.network.recv(1024)
        msg = str(bmsg,"utf-8")
        if msg == "Metadados chegaram":
            return 1
        elif msg == "Arquivo existe":
            return 2
        else:
            return 0

    def sendFile(self, filepath, filesize):
        chunck = 1024
        max_chunk = filesize/chunck
        try:
            file = open(filepath, 'rb')
            progess = QProgressDialog("Transferindo arquivo para o servidor solicitado","cancelar",0, max_chunk)
            progess.setWindowModality(Qt.WindowModal)

            bytesToSend = file.read(chunck)
            self.network.send(bytesToSend)

            progess.setValue(chunck)
            while bytesToSend != b'':
                bytesToSend = file.read(chunck)
                self.network.send(bytesToSend)
                progess.setValue(progess.value() + chunck)
            return 1
        except FileNotFoundError:
            return 0
        except socket.error:
            return 0
        finally:
            file.flush()
            file.close()
            self.network.close()



