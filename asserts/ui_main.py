# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'main.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Main(object):
    def setupUi(self, Main):
        Main.setObjectName("Main")
        Main.resize(522, 443)
        self.horizontalLayout = QtWidgets.QHBoxLayout(Main)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.treeView = QtWidgets.QTreeView(Main)
        self.treeView.setObjectName("treeView")
        self.horizontalLayout.addWidget(self.treeView)
        self.formLayout_2 = QtWidgets.QFormLayout()
        self.formLayout_2.setLabelAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.formLayout_2.setObjectName("formLayout_2")
        self.label = QtWidgets.QLabel(Main)
        self.label.setObjectName("label")
        self.formLayout_2.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)
        self.labelName = QtWidgets.QLabel(Main)
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        font.setPointSize(16)
        self.labelName.setFont(font)
        self.labelName.setText("")
        self.labelName.setObjectName("labelName")
        self.formLayout_2.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.labelName)
        self.label_2 = QtWidgets.QLabel(Main)
        self.label_2.setObjectName("label_2")
        self.formLayout_2.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_2)
        self.labelSize = QtWidgets.QLabel(Main)
        font = QtGui.QFont()
        font.setFamily("Noto Sans")
        font.setPointSize(16)
        self.labelSize.setFont(font)
        self.labelSize.setText("")
        self.labelSize.setObjectName("labelSize")
        self.formLayout_2.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.labelSize)
        self.label_3 = QtWidgets.QLabel(Main)
        self.label_3.setObjectName("label_3")
        self.formLayout_2.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.label_3)
        self.txtIp = QtWidgets.QLineEdit(Main)
        self.txtIp.setObjectName("txtIp")
        self.formLayout_2.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.txtIp)
        self.btnCancel = QtWidgets.QPushButton(Main)
        self.btnCancel.setObjectName("btnCancel")
        self.formLayout_2.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.btnCancel)
        self.btnSend = QtWidgets.QPushButton(Main)
        self.btnSend.setObjectName("btnSend")
        self.formLayout_2.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.btnSend)
        self.horizontalLayout.addLayout(self.formLayout_2)

        self.retranslateUi(Main)
        QtCore.QMetaObject.connectSlotsByName(Main)

    def retranslateUi(self, Main):
        _translate = QtCore.QCoreApplication.translate
        Main.setWindowTitle(_translate("Main", "Form"))
        self.label.setText(_translate("Main", "Nome do arquivo:"))
        self.label_2.setText(_translate("Main", "Tamanho do arquivo:"))
        self.label_3.setText(_translate("Main", "Ip de conexão:"))
        self.btnCancel.setText(_translate("Main", "Cancelar"))
        self.btnSend.setText(_translate("Main", "Enviar"))

