from PyQt5.QtWidgets import QWidget, QFileSystemModel, QMessageBox
from asserts.ui_main import Ui_Main
from utils.network import NetworkSocket

class MainWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.ui = Ui_Main()
        self.ui.setupUi(self);
        self.setWindowTitle("Programa desktop para trasnferência de Arquivo")

        self.model = QFileSystemModel()
        self.model.setRootPath("/home/josias/")
        self.ui.treeView.setModel(self.model)

        self.metaData = []

        self.ui.treeView.doubleClicked.connect(self.changeSelect)
        self.ui.btnSend.clicked.connect(self.sendFile)
        self.ui.btnCancel.clicked.connect(self.cancelSend)

    def changeSelect(self, index):
        if not self.model.isDir(index):
            self.metaData = self.model.fileInfo(index)
            filename = self.metaData.fileName()
            filesize = self.metaData.size()
            self.ui.labelName.setText(filename)
            self.ui.labelSize.setText(str(filesize) + ' Bytes')

    def sendFile(self):
        if self.metaData == [] or self.ui.txtIp.text() == '':
            msgText = 'Você não selecionou nenhum arquivo. Por favor, selecione um arquivo.' if self.metaData == [] else 'Digite um endereço ip'
            self.showBox(msgText,"Erro na configuração para envio", QMessageBox.Warning)
        else:
            conn = NetworkSocket(self.ui.txtIp.text())
            fileServerState = conn.sendMetaData(self.metaData.fileName())
            if fileServerState == 1:
                res = conn.sendFile(self.metaData.absoluteFilePath(),self.metaData.size())
                self.showBox("Enviado com sucesso","Sucesso", QMessageBox.Information) if res == 1 else self.showBox("Falha ao enviar o arquivo","Falha", QMessageBox.Critical)
            elif fileServerState == 2:
                self.showBox("Arquivo já existe no destino", "informação repetida", QMessageBox.Warning)
            else:
                self.showBox("Erro ao enviar os metadados", "Erro de metadados", QMessageBox.Warning)


    def showBox(self,message,title,icon):
        alert = QMessageBox()
        alert.setIcon(icon)
        alert.setText(message)
        alert.setWindowTitle(title)
        alert.show()
        alert.exec_()

    def cancelSend(self):
        self.metaData = []
        self.ui.txtIp.setText('')
        self.ui.labelName.setText('')
        self.ui.labelSize.setText('')